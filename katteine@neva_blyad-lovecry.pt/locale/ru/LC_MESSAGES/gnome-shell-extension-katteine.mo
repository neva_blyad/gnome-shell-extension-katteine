��          �      L      �  H   �     
            0   /     `     y  /   �  (   �     �     �          1     L     [  (   n  #   �     �  �  �  v   �       %   *  %   P  ]   v  C   �  '     d   @  L   �  1   �  D   $  B   i  <   �  !   �  '   	  Z   3	  <   �	  >   �	           	                                  
                                                       A list of strings, each containing an application id (desktop file name) Add Add application Application list Applications which enable Katteine automatically Create new matching rule Enable notifications Enable when a fullscreen application is running Have a rest with your dear Kate if tired Now it's time to work Restore katteine state Restore state across reboots Show Katteine in top panel Show indicator Show notifications Show notifications when enabled/disabled Show the indicator on the top panel Store katteine user state Project-Id-Version: gnome-shell-extension-katteine
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-07-30 15:53+0300
Last-Translator: Alex Gluck <alexgluck@bk.ru>
Language-Team: Russian
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n>1);
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.0.2
X-Poedit-SearchPath-0: ../../..
 Список строк, содержащих название программы (включая файлы .desktop) Добавить Добавить приложение Добавить приложение Приложения, которые активируют Katteine автоматически Создайте новое правило соответствия Включить уведомления Включать при работе приложений в полноэкранном режиме Настало время подумать о твоей милой Кате Теперь пора снова работать Восстанавливать значения приложения Восстанавливать после перезагрузки Отображать значёк Katteine на панели Показывать значок Включить уведомления Показывать уведомление когда включено/выключено Отображать значок Katteine на панели Сохранять пользовательский выбор 