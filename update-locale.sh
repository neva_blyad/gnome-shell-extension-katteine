#!/bin/bash

cd katteine@neva_blyad-lovecry.pt

pot=gnome-shell-extension-katteine.pot

touch $pot
xgettext --from-code=utf-8 -j *.js -o $pot
xgettext --from-code=utf-8 -j schemas/*.xml -o $pot

for locale_lang in locale/*; do
    po=$locale_lang/LC_MESSAGES/gnome-shell-extension-katteine.po
    echo $po
    msgmerge --backup=off -U $po $pot
    msgfmt $po -o ${po%po}mo
done

rm $pot
