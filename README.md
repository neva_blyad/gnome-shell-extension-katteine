# NAME

Katteine

# VERSION

0.03

# DESCRIPTION

Have a rest with your dear Kate if tired.

gnome-shell-extension-caffeine is original extension.

# SCREENSHOTS

![Alt Text](screenshot1.png "Katteine screenshot 1")
![Alt Text](screenshot2.png "Katteine screenshot 2")

# COPYRIGHT AND LICENSE

    Copyright (C) 2011-2012 Giovanni Campagna <scampa.giovanni@gmail.com>
    Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                            <neva_blyad@lovecri.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# AUTHORS

    Giovanni Campagna <scampa.giovanni@gmail.com>
    НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                    <neva_blyad@lovecri.es>
    Invisible Light
